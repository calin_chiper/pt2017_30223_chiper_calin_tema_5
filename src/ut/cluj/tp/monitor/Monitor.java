package ut.cluj.tp.monitor;



import java.util.List;

public interface Monitor<T> {
    List<T> fetchData();
}
