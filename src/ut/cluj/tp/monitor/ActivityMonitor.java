package ut.cluj.tp.monitor;


import ut.cluj.tp.model.MonitoredData;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


public class ActivityMonitor implements Monitor<MonitoredData> {

    private final String SENSOR_DATA = "./resources/Activities.txt";

    public List<MonitoredData> fetchData()  {
        try {

            //noinspection Convert2MethodRef
            return Files.lines(Paths.get(SENSOR_DATA))
                        .map(line -> parse(line))
                        .collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }


    private MonitoredData parse(final String line) {
        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        final String[] data= line.split("\t\t");
        return new MonitoredData(LocalDateTime.parse(data[0], dateTimeFormatter),        //startTime
                                LocalDateTime.parse(data[1], dateTimeFormatter),         //endTime
                                data[2].replace("\t", ""));            //activity
    }

}

