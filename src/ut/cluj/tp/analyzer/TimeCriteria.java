package ut.cluj.tp.analyzer;


import ut.cluj.tp.model.DateTime;

import java.util.Map;

public interface TimeCriteria {
    Map<String, DateTime> meetCriteria(Map<String, Integer> durationForEachActivity);
}
