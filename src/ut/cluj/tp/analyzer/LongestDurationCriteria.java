package ut.cluj.tp.analyzer;

import ut.cluj.tp.model.DateTime;

import java.util.Map;
import java.util.stream.Collectors;


public class LongestDurationCriteria implements TimeCriteria {
    @Override
    public Map<String, DateTime> meetCriteria(Map<String, Integer> durationForEachActivity) {
        return durationForEachActivity.entrySet().stream()
                .filter(entry -> isLonger(entry.getValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> DateTime.ofSeconds(entry.getValue())));
    }

    private boolean isLonger(int seconds) {
        DateTime dateTime = DateTime.ofSeconds(seconds);
        return dateTime.getDays() > 0 || dateTime.getHours() > 10;
    }
}
