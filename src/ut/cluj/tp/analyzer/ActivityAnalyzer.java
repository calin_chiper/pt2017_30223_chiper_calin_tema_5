package ut.cluj.tp.analyzer;

import ut.cluj.tp.model.DateTime;
import ut.cluj.tp.model.MonitoredData;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.*;


public class ActivityAnalyzer {
    private final List<MonitoredData> monitoredData;

    public ActivityAnalyzer(List<MonitoredData> monitoredData) {
        this.monitoredData = monitoredData;
    }

    /**
     * Counts the distinct days that appears in the monitoring data
     * @return number of distinct days
     */
    public long countDistinctDays() {
        return this.monitoredData.stream()
                .map(data -> data.getStartTime().getDayOfYear())
                .distinct()
                .count();
    }

    /**
     * Counts the total occurrences of every activity
     * @return a map described by an activity used as a key and the number of occurrence of that activity as a value
     */
    public Map<String, Long> countTotalActivityOccurrence() {
        return this.monitoredData.stream()
                .map(MonitoredData::getActivity)
                .collect(groupingBy(activity -> activity, counting()));
    }

    /**
     * Counts the daily occurrences of every activity
     * @return a map described by a day as a key and a map( Key = Activity, Value = Num. of occurrence) as a value
     */
    public Map<Integer, Map<String, Long>> countDailyActivityOccurrence() {
        //noinspection Convert2MethodRef
        return this.monitoredData.stream()
                .collect(groupingBy(data -> getDay(data), groupingBy(MonitoredData::getActivity, counting())));
    }



    private Integer getDay(final MonitoredData data) {
        return data.getStartTime().getDayOfMonth();
    }

    /**
     * Computes the total duration in seconds for each activity over the monitoring period
     * @return a map described by the activity as a key and the total duration for that activity as value
     */
    public Map<String, Integer> computeTotalDurationForEachActivity() {
        return this.monitoredData.stream()
                .collect(groupingBy(MonitoredData::getActivity,
                         reducing(0, MonitoredData::getDuration, Integer::sum)));
    }

    /**
     * Filters the activities with total duration larger than 10 hours
     * @return a map described by the activity as a key and a DateTime object as value
     */
    public Map<String, DateTime> getMostFrequentActivities() {
        return new LongestDurationCriteria().meetCriteria(this.computeTotalDurationForEachActivity());
    }

    /**
     * Filters the activities that have 90% of duration less than 5 minutes
     * @return a list of the shortest activities
     */
    public List<String> getShortestActivities() {
    //noinspection Convert2MethodRef
    return this.monitoredData.stream()
            .collect(groupingBy(MonitoredData::getActivity)).entrySet().stream()
            .collect(toMap(Map.Entry::getKey, entry -> getDurationForEveryEntry(entry))).entrySet().stream()
            .filter(entry -> isShorter(entry.getValue()))
            .map(Map.Entry::getKey)
            .collect(toList());
}

    private List<Integer> getDurationForEveryEntry(Map.Entry<String, List<MonitoredData>> entry) {
        return entry.getValue().stream()
                .map(MonitoredData::getDuration)
                .collect(toList());
    }

    private boolean isShorter(final List<Integer> durations) {
        return durations.stream()
                .sorted()
                .limit(Double.valueOf(durations.size() * 0.9).longValue())
                .allMatch(duration -> DateTime.ofSeconds(duration).getMinutes() < 5);
    }

}
