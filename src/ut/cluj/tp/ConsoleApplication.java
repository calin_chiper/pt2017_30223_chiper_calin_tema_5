package ut.cluj.tp;


import ut.cluj.tp.analyzer.ActivityAnalyzer;
import ut.cluj.tp.model.MonitoredData;
import ut.cluj.tp.monitor.ActivityMonitor;
import ut.cluj.tp.monitor.Monitor;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

public class ConsoleApplication {
    private boolean exit;
    private Monitor<MonitoredData> monitor;
    private ActivityAnalyzer analyzer;
    private PrintStream printStream;

    private ConsoleApplication() {
        this.exit = false;
        this.monitor = new ActivityMonitor();
        this.analyzer = new ActivityAnalyzer(this.monitor.fetchData());
    }

    public static void main(String[] args) {
        new ConsoleApplication().run();
    }

    private void init() {
        try {
            this.printStream = new PrintStream("resources/log.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    private void run() {
        init();
        while(!exit) {
            printMenuHeader();
            printMenuOptions();
            printInput();
            int option = chooseOption();
            runOption(option);
        }
    }

    private void runOption(int option) {
        switch (option) {
            case 0:
                this.exit = true;
                System.out.println("Exiting...");
                break;
            case 1:
                printDistinctDays();
                printStream.println("\nDistinct days: " + this.analyzer.countDistinctDays());
                break;
            case 2:
                printTotalOccurrence();
                printStream.println("\nTotal occurrence: " + this.analyzer.countTotalActivityOccurrence());
                break;
            case 3:
                printDailyOccurrence();
                printStream.println("\nDaily occurrence: " + this.analyzer.countDailyActivityOccurrence());
                break;
            case 4:
                printTotalDuration();
                printStream.println("\nTotal durations for each activity: " + this.analyzer.computeTotalDurationForEachActivity());
                break;
            case 5:
                printMostFrequentActivities();
                printStream.println("\nMost Frequent activities : " + this.analyzer.getMostFrequentActivities());
                break;
            case 6:
                printShortestActivities();
                printStream.println("\nShortest activities : " + this.analyzer.getShortestActivities());

                break;
            default:
                System.out.println("\nInvalid selected option.\n");
        }
    }

    private void printShortestActivities() {
        System.out.println("\nShortest activities: ");
        String leftAlignFormat = "| %-15s |%n";
        System.out.format("+-----------------+%n");
        System.out.format("| Activity        |%n");
        System.out.format("+-----------------+%n");
        this.analyzer.getShortestActivities().forEach(activity ->
                System.out.format(leftAlignFormat, activity));
        System.out.format("+-----------------+%n");
        System.out.println("\n");
    }

    private void printMostFrequentActivities() {
        System.out.println("\nMost frequent activities: ");
        String leftAlignFormat = "| %-15s | %-11s |%n";
        System.out.format("+-----------------+-----------------+%n");
        System.out.format("| Activity        |   Time          |%n");
        System.out.format("+-----------------+-----------------+%n");
        this.analyzer.getMostFrequentActivities().forEach((activity, time) ->
                System.out.format(leftAlignFormat, activity, time));
        System.out.format("+-----------------+-----------------+%n");
    }

    private void printTotalDuration() {
        System.out.println("\nTotal duration for each activity: ");
        String leftAlignFormat = "| %-15s | %-13d |%n";
        System.out.format("+-----------------+---------------+%n");
        System.out.format("| Activity        |   Seconds     |%n");
        System.out.format("+-----------------+---------------+%n");
        this.analyzer.computeTotalDurationForEachActivity().forEach((activity, seconds ) ->
                System.out.format(leftAlignFormat,  activity, seconds));
        System.out.format("+-----------------+---------------+%n");
    }

    private void printDailyOccurrence() {
        System.out.println("\nDaily occurrence for each activity: ");
        this.analyzer.countDailyActivityOccurrence().forEach((day, occurrenceMap) -> {
            System.out.format("+-----------------+---------------+%n");
            System.out.println("\nDay " + day + ":");
            String leftAlignFormat = "| %-15s | %-13d |%n";
            System.out.format("+-----------------+---------------+%n");
            System.out.format("| Activity        |   Occurrence  |%n");
            System.out.format("+-----------------+---------------+%n");
            occurrenceMap.forEach((activity , dailyOccurrence) ->
                    System.out.format(leftAlignFormat, activity, dailyOccurrence));
        });
        System.out.format("+-----------------+---------------+%n");
    }

    private void printTotalOccurrence() {
        System.out.println("\nTotal occurrence for each activity: ");
        String leftAlignFormat = "| %-15s | %-13d |%n";
        System.out.format("+-----------------+---------------+%n");
        System.out.format("| Activity        |   Occurrence  |%n");
        System.out.format("+-----------------+---------------+%n");
        this.analyzer.countTotalActivityOccurrence()
                .forEach((activity , occurrence) ->
                        System.out.format(leftAlignFormat, activity, occurrence));
        System.out.format("+-----------------+---------------+%n");
    }

    private void printDistinctDays() {
        System.out.print("\nDistinct Days: ");
        System.out.println(this.analyzer.countDistinctDays());
        System.out.println("\n");
    }

    private void printMenuHeader() {
        System.out.println("+-------------+");
        System.out.println("|  Functional |");
        System.out.println("|     data    |");
        System.out.println("|  processing |");
        System.out.println("+-------------+");
    }

    private void printInput() {
        System.out.print("> ");
    }

    private void printMenuOptions() {
        System.out.println("1) Count distinct days");
        System.out.println("2) Count the total occurrence for each activity");
        System.out.println("3) Count the daily occurrence for each activity");
        System.out.println("4) Compute total duration for each activity");
        System.out.println("5) Show most frequent activities");
        System.out.println("6) Show the shortest activities");
        System.out.println("0) Exit");
    }

    private int chooseOption() {
        Scanner keyboard = new Scanner(System.in);
        return keyboard.nextInt();
    }


}
