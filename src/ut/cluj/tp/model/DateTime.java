package ut.cluj.tp.model;


import java.util.concurrent.TimeUnit;


public class DateTime {
    private long days;
    private long hours;
    private long minutes;
    private long seconds;
    private DateTime dateTime;

    private DateTime(int seconds) {
        this.days = (int) TimeUnit.SECONDS.toDays(seconds);
        this.hours = TimeUnit.SECONDS.toHours(seconds) - TimeUnit.DAYS.toHours(this.days);
        this.minutes = TimeUnit.SECONDS.toMinutes(seconds) - TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(seconds));
        this.seconds = TimeUnit.SECONDS.toSeconds(seconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(seconds));
    }

    public static DateTime ofSeconds(int seconds) {
        return new DateTime(seconds);
    }

    @Override
    public String toString() {
        String seconds = String.valueOf(this.seconds);
        String minutes = String.valueOf(this.minutes);
        String hours = String.valueOf(this.hours);

        if(this.seconds < 10 && this.seconds >= 0) seconds = "0" + this.seconds;
        if(this.minutes < 10 && this.minutes >= 0) minutes = "0" + this.minutes;
        if(this.hours < 10 && this.hours >= 0) hours = "0" + this.hours;

        if(this.days == 0) {
            return hours + ":" + minutes + ":" + seconds;
        } else {
            return this.days + " days " + hours + ":" + minutes + ":" + seconds;
        }

    }

    public long getDays() {
        return days;
    }

    public long getHours() {
        return hours;
    }

    public long getMinutes() {
        return minutes;
    }

    public long getSeconds() {
        return seconds;
    }
}
